# What we have here ?

This file contains css code i use in my firefox chrome to change the look 
of my browser. This code has been taken from this repository "https://github.com/MrOtherGuy/firefox-csshacks". And mixed to my taste. 

# What is the purpose of this ?

* Backup this file 
* Share this file with my friends and the world

# Disclaimer 

I am not a css developer i just copy code to make my firefox looks like i want it. The code original source is in MrOtherGuy github. I am also new to git so if i do something wrong please let me know.


# Screen shot of my Firefox browser

![IMAGE_DESCRIPTION](./screenshot.png)